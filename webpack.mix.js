const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
    'resources/assets/admin/plugins/fontawesome-free/css/all.min.css',
    'resources/assets/admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css',
    'resources/assets/admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css',
    'resources/assets/admin/plugins/daterangepicker/daterangepicker.css',
    'resources/assets/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css',
    'resources/assets/admin/plugins/select2/css/select2.min.css',
    'resources/assets/admin/plugins/summernote/summernote-bs4.css',
    'resources/assets/admin/css/adminlte.min.css'
], 'public/css/admin.css');

mix.scripts([
    'resources/assets/admin/plugins/jquery/jquery.min.js',
    'resources/assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js',
    'resources/assets/admin/plugins/datatables/jquery.dataTables.min.js',
    'resources/assets/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js',
    'resources/assets/admin/plugins/datatables-responsive/js/dataTables.responsive.min.js',
    'resources/assets/admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js',
    'resources/assets/admin/plugins/moment/moment.min.js',
    'resources/assets/admin/plugins/daterangepicker/daterangepicker.js',
    'resources/assets/admin/plugins/select2/js/select2.full.min.js',
    'resources/assets/admin/plugins/summernote/summernote-bs4.min.js',
    'resources/assets/admin/js/adminlte.min.js'
], 'public/js/admin.js');

mix.copy('resources/assets/admin/plugins/fontawesome-free/webfonts', 'public/webfonts');
mix.copy('resources/assets/admin/plugins/summernote/font', 'public/css/font');
mix.copy('resources/assets/admin/images', 'public/images/AdminLTE');

mix.styles([
    'resources/assets/css/bootstrap.min.css',
    'resources/assets/css/font-awesome.min.css',
    'resources/assets/css/style.css'
], 'public/css/app.css');

mix.scripts([
    'resources/assets/js/jquery.min.js',
    'resources/assets/js/bootstrap.bundle.min.js'
], 'public/js/app.js');

mix.copy('resources/assets/images', 'public/images');
mix.copy('resources/assets/fonts', 'public/fonts');
