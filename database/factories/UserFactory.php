<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => 'Администратор',
        'email' => 'Link1518@yandex.ru',
        'password' => bcrypt('1234567'),
        'status' => 1,
        'admin' => 1
    ];
});
