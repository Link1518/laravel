<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Site;
use Faker\Generator as Faker;

$factory->define(Site::class, function (Faker $faker) {
    return [
        'name' => 'ООО «КОРВЕТ»',
        'mail_to' => 'Link1518@yandex.ru',
        'address' => 'г. Набережные Челны, ул. Низаметдинова, 22',
        'phone' => '8-937-280-78-78',
        'email' => 'korvet-rt@mail.ru'
    ];
});
