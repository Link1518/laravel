<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Page;
use App\Site;
use App\User;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view) {
            $view->with('site', Site::find(1));
        });

        view()->composer('admin.*', function($view) {
            $view->with('user', Auth::user());
            $view->with('menu', Page::getAdminMenu());
        });

        view()->composer('app.*', function($view) {
            $view->with('menu', Page::getAppMenu());
        });
    }
}
