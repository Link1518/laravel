<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Site extends Model
{
    protected $fillable = [
        'name', 'mail_to', 'address', 'phone', 'email', 'banner_text', 'about_us_text', 'footer_text'
    ];

    public static function add($fields)
    {
        $site = new static;

        $site->fill($fields);
        $site->save();

        return $site;
    }

    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }

    public function uploadImage($image, $field)
    {
        if ($image == null) return;

        $this->removeImage($field);
        $filename = str_random(10) . '.' . $image->extension();
        $image->storeAs('public/settings', $filename);
        $this->$field = $filename;
        $this->save();
    }

    public function removeImage($field)
    {
        if ($this->$field != null) Storage::delete('public/settings/' . $this->$field);
    }

    public function getImage($field)
    {
        if ($this->$field == null) return '/images/no_image_320x240.jpg';
        else return Storage::url('settings/' . $this->$field);
    }
}
