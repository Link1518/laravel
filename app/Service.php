<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Service extends Model
{
    protected $fillable = [
        'name', 'price'
    ];

    public static function add($fields)
    {
        $site = new static;

        $site->fill($fields);
        $site->save();

        return $site;
    }

    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }

    public function remove()
    {
        $this->removeImage('image');
        $this->delete();
    }

    public function uploadImage($image, $field)
    {
        if ($image == null) return;

        $this->removeImage($field);
        $filename = str_random(10) . '.' . $image->extension();
        $image->storeAs('public/services', $filename);
        $this->$field = $filename;
        $this->save();
    }

    public function removeImage($field)
    {
        if ($this->$field != null) Storage::delete('public/services/' . $this->$field);
    }

    public function getImage($field)
    {
        if ($this->$field == null) return '/images/no_image_320x240.jpg';
        else return Storage::url('services/' . $this->$field);
    }
}
