<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email'
    ];

    protected $hidden = [
        'password', 'remember_token'
    ];

    protected $casts = [
        'email_verified_at' => 'datetime'
    ];

    public static function add($fields)
    {
        $user = new static;

        $user->fill($fields);
        $user->save();

        return $user;
    }

    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }

    public function remove()
    {
        $this->delete();
    }

    public function generatePassword($password)
    {
        if ($password != null)
        {
            $this->password = bcrypt($password);
            $this->save();
        }
    }

    public function setStatus($value)
    {
        if ($value == 1) $this->status = 1;
        else $this->status = 0;

        $this->save();
    }

    public function isActive()
    {
        if ($this->status == 1) return 'Да';
        else return 'Нет';
    }

    public function isAdmin()
    {
        if ($this->admin == 1) return 'Да';
        else return 'Нет';
    }
}
