<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Site;

class SettingsController extends Controller
{
    public function index()
    {
        $site = Site::find(1);

        return view('admin.settings', compact('site'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'mail_to' => 'required|email',
            'logo' => 'nullable|image',
            'address' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'banner_image' => 'nullable|image',
            'about_us_image' => 'nullable|image'
        ]);

        $site = Site::find(1);
        $site->edit($request->all());
        $site->uploadImage($request->file('logo'), 'logo');
        $site->uploadImage($request->file('banner_image'), 'banner_image');
        $site->uploadImage($request->file('about_us_image'), 'about_us_image');

        return redirect('/admin');
    }
}
