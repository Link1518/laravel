<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;

class PagesController extends Controller
{
    public function index()
    {
        $pages = Page::all();

        return view('admin.pages.index', compact('pages'));
    }

    public function create()
    {
        $pages = Page::getPages();

        return view('admin.pages.create', compact('pages'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'sort' => 'integer'
        ]);

        $page = Page::add($request->all());
        $page->setStatus($request->get('status'));

        return redirect()->route('pages.index');
    }

    public function edit($id)
    {
        $page = Page::find($id);
        $pages = Page::getPages();

        return view('admin.pages.edit', compact('page', 'pages'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'code' => 'required',
            'sort' => 'integer'
        ]);

        $page = Page::find($id);
        $page->edit($request->all());
        $page->setStatus($request->get('status'));

        return redirect()->route('pages.index');
    }

    public function destroy($id)
    {
        Page::find($id)->remove();
    }
}
