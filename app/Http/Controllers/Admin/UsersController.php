<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\User;
use Auth;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::all();

        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);

        $user = User::add($request->all());
        $user->generatePassword($request->get('password'));
        $user->setStatus(1);

        return redirect()->route('users.index');
    }

    public function edit($id)
    {
        $user = User::find($id);

        return view('admin.users.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users')->ignore($id)]
        ]);

        $user = User::find($id);
        $user->edit($request->all());
        $user->generatePassword($request->get('password'));
        $user->setStatus($request->get('status'));

        return redirect()->route('users.index');
    }

    public function destroy($id)
    {
        User::find($id)->remove();
    }

    public function login()
    {
        return view('admin.login');
    }

    public function loginAuth(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $auth = Auth::attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password')
        ]);

        if ($auth) return redirect('/admin');
        else return redirect()->back()->with('error', 'Неправильный логин или пароль.');
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('login');
    }
}
