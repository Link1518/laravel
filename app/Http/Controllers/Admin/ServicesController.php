<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;

class ServicesController extends Controller
{
    public function index()
    {
        $services = Service::all();

        return view('admin.services.index', compact('services'));
    }

    public function create()
    {
        return view('admin.services.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'image' => 'nullable|image'
        ]);

        $service = Service::add($request->all());
        $service->uploadImage($request->file('image'), 'image');

        return redirect()->route('services.index');
    }

    public function edit($id)
    {
        $service = Service::find($id);

        return view('admin.services.edit', compact('service'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'image' => 'nullable|image'
        ]);

        $service = Service::find($id);
        $service->edit($request->all());
        $service->uploadImage($request->file('image'), 'image');

        return redirect()->route('services.index');
    }

    public function destroy($id)
    {
        Service::find($id)->remove();
    }
}
