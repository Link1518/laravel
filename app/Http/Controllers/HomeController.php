<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Service;
use App\Site;

class HomeController extends Controller
{
    public function index()
    {
        $services = Service::all();

        return view('app.home', compact('services'));
    }

    public function page($parent, $child = false)
    {
        $parent = Page::where('code', $parent)->firstOrFail();

        if ($child)
        {
            $child = Page::where('code', $child)->firstOrFail();
            if ($child->parent_id != $parent->id) abort(404);
            $page = $child;
        }
        else
        {
            $page = $parent;
        }

        return view('app.page', compact('page'));
    }

    public function send(Request $request)
    {
        $site = Site::find(1);

        $to = $site->mail_to;

        $subject = 'Заказ звонка';

        $message = $subject . ' с сайта ' . $_SERVER['HTTP_HOST'] . '<br><br>';
        $message .= 'Имя: ' . $request->get('name') . '<br />';
        $message .= 'Номер телефона: ' . $request->get('phone') . '<br />';
        if ($request->get('text')) $message .= 'Комментарий: ' . $request->get('text') . '<br />';
        if ($request->get('time')) $message .= 'Удобное время звонка: ' . $request->get('time') . '<br />';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type: text/html; charset=utf-8" . "\r\n";
        $headers .= "From: Корвет <noreply@$_SERVER[HTTP_HOST]>" . "\r\n";

        mail($to, $subject, $message, $headers);
    }
}
