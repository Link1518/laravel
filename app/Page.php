<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Auth;

class Page extends Model
{
    use Sluggable;

    protected $fillable = [
        'name', 'code', 'content', 'parent_id', 'sort', 'seo_title', 'seo_description', 'seo_keywords'
    ];

    public function sluggable()
    {
        return [
            'code' => [
                'source' => 'name'
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('sort', 'asc');
        });
    }

    public static function add($fields)
    {
        $page = new static;

        $page->fill($fields);
        $page->user_id = Auth::user()->id;
        $page->save();

        return $page;
    }

    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }

    public function setStatus($value)
    {
        if ($value != null) $this->status = 1;
        else $this->status = 0;

        $this->save();
    }

    public static function getPages()
    {
        $pages = Page::where('parent_id', 0)->pluck('name', 'id')->all();
        $pages = ['Корневой раздел'] + $pages;

        return $pages;
    }

    public function getChilds()
    {
        $pages = Page::where('parent_id', $this->id)->select('id', 'name', 'code')->get();

        $childs = [];

        foreach ($pages as $page)
        {
            $childs[] = [
                'id' => $page->id,
                'name' => $page->name,
                'href' => '/' . $this->code . '/' . $page->code
            ];
        }

        return $childs;
    }

    public static function getAdminMenu()
    {
        $pages = Page::where('parent_id', 0)->select('id', 'name', 'code')->get();

        foreach ($pages as $page)
        {
            $menu[] = [
                'name' => $page->name,
                'href' => route('pages.edit', $page->id),
                'childs' => $page->getChilds()
            ];
        }

        return $menu;
    }

    public static function getAppMenu()
    {
        $pages = Page::where('parent_id', 0)->select('name', 'code')->get();

        $menu[] = [
            'name' => 'Главная',
            'href' => '/'
        ];

        foreach ($pages as $page)
        {
            $menu[] = [
                'name' => $page->name,
                'href' => '/' . $page->code
            ];
        }

        return $menu;
    }
}
