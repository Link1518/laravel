$(function() {
    $('.back-to-top').on('click', function() {
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
        return false;
    });

    $('.js-feedback').on('submit', function() {
        var name = $(this).find('.js-name').val();
        var phone = $(this).find('.js-phone').val();

        if (name || phone)
        {
            $.ajax({
                url: '/send',
                type: 'post',
                data: $(this).serialize() + '&_token=' + $('meta[name="csrf-token"]').attr('content'),
                dataType: 'json',
                success: function() {
                    alert('Спасибо, Ваша заявка отправлена.');
                    location.reload();
                }
            });
        }
        else
        {
            alert('Пожалуйста, заполните все поля.');
        }

        return false;
    });
});