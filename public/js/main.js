$(function() {
    $('#list').DataTable({
        'autoWidth': false,
        'info': true,
        'lengthChange': true,
        'ordering': true,
        'paging': true,
        'searching': true,
        'responsive': true
    });

    $('.js-date').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD.MM.YYYY'
        }
    });

    $('.js-editor').summernote();

    $('.js-select').select2();

    $('.js-remove').on('click', function() {
        if (!confirm('Вы уверены, что хотите удалить?')) return false;

        $.ajax({
            url: $(this).data('url'),
            type: 'delete',
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function() {
                location.reload();
            }
        });

        return false;
    });
});