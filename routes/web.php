<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'guest'], function() {
    Route::get('/login', 'UsersController@login')->name('login');
    Route::post('/login', 'UsersController@loginAuth')->name('loginAuth');
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin'], function() {
    Route::get('/', 'DashboardController@index');
    Route::get('/logout', 'UsersController@logout')->name('logout');
    Route::get('/settings', 'SettingsController@index')->name('settings.index');
    Route::post('/settings', 'SettingsController@update')->name('settings.update');
    Route::resource('/feedbacks', 'FeedbacksController');
    Route::resource('/pages', 'PagesController');
    Route::resource('/services', 'ServicesController');
    Route::resource('/users', 'UsersController');
});

Route::post('/send', 'HomeController@send');

Route::get('/{parent}', 'HomeController@page');
Route::get('/{parent}/{child}', 'HomeController@page');
