@extends('app.layout', [
    'title' => 'Страница не найдена',
    'description' => '',
    'keywords' => ''
])

@section('content')
<div class="container">
    <h1 class="title">Страница не найдена</h1>
    <div class="content">
        <p>Возможно, Вы попытались загрузить несуществующую или удаленную страницу.</p>
        <p>Перейти на <a href="/">главную страницу</a> сайта.</p>
    </div>
</div>
@endsection