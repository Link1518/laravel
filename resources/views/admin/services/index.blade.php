@extends('admin.layout', ['title' => 'Услуги и цены'])

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <a href="{{ route('services.create') }}" class="btn bg-gradient-success">Добавить</a>
            </div>
            <div class="card-body">
                <table id="list" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <th>Изображение</th>
                            <th>Цена</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($services as $service)
                        <tr>
                            <td>{{ $service->id }}</td>
                            <td><a href="{{ route('services.edit', $service->id) }}">{{ $service->name }}</a></td>
                            <td><img src="{{ $service->getImage('image') }}" alt="" class="preview-image"></td>
                            <td>{{ $service->price }}</td>
                            <td>
                                <a href="{{ route('services.edit', $service->id) }}" class="btn bg-gradient-warning"
                                    title="Изменить"
                                >
                                    <i class="fas fa-edit"></i>
                                </a>
                                <a href="#" class="btn bg-gradient-danger js-remove"
                                    data-url="{{ route('services.destroy', $service->id) }}"
                                    title="Удалить"
                                >
                                    <i class="fas fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection