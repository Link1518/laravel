@extends('admin.layout', ['title' => 'Новая услуга - Добавление'])

@section('content')
{{ Form::open(['route' => 'services.store', 'files' => true]) }}
    <div class="card card-secondary">
        <div class="card-header">
            <h3 class="card-title">Информация</h3>
        </div>
        <div class="card-body">
            @include('admin.errors')
            <div class="row">
                <div class="col-md-4">
                    <p class="text-bold text-md-right mt-1">Название</p>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <p class="text-bold text-md-right mt-1">Изображение</p>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="file" class="form-control-file" name="image">
                    </div>
                </div>
                <div class="col-md-4">
                    <p class="text-bold text-md-right mt-1">Цена</p>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" class="form-control" name="price" value="{{ old('price') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn bg-gradient-success">Сохранить</button>
            <a href="{{ route('services.index') }}" class="btn bg-gradient-secondary">Отменить</a>
        </div>
    </div>
{{ Form::close() }}
@endsection