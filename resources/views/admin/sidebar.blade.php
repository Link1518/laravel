<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="/admin" class="brand-link">
        <i class="fas fa-balance-scale my-1 mr-2 ml-3"></i>
        <span class="brand-text font-weight-light">{{ $site->name }}</span>
    </a>
    <div class="sidebar">
        <div class="user-panel pb-3 mt-3 mb-3 d-flex">
            <div class="image">
                <img src="/images/AdminLTE/user2-160x160.jpg" alt="" class="img-circle elevation-2">
            </div>
            <div class="info">
                <a href="/admin" class="d-block">{{ $user->name }}</a>
            </div>
        </div>
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                @foreach ($menu as $item)
                <li class="nav-item has-treeview">
                    <a href="{{ $item['href'] }}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>{{ $item['name'] }}<i class="fas fa-angle-left right"></i></p>
                    </a>
                    @if ($item['childs'])
                    <ul class="nav nav-treeview">
                        @foreach ($item['childs'] as $child)
                        <li class="nav-item">
                            <a href="{{ route('pages.edit', $child['id']) }}" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>{{ $child['name'] }}</p>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                    @endif
                </li>
                @endforeach
                <li class="nav-item has-treeview">
                    <a href="{{ route('pages.create') }}" class="nav-link active">
                        <i class="nav-icon fas fa-plus"></i>
                        <p>Добавить</p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>