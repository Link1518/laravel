@extends('admin.layout', ['title' => 'Настройки'])

@section('content')
{{ Form::open(['route' => 'settings.update', 'files' => true]) }}
    <div class="card card-secondary">
        <div class="card-header">
            <h3 class="card-title">Информация</h3>
        </div>
        <div class="card-body">
            @include('admin.errors')
            <div class="row">
                <div class="col-md-4">
                    <p class="text-bold text-md-right mt-1">Название</p>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" value="{{ $site->name }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <p class="text-bold text-md-right mt-1">E-mail получателя писем</p>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" class="form-control" name="mail_to" value="{{ $site->mail_to }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <p class="text-bold text-md-right mt-1">Логотип</p>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <p><img src="{{ $site->getImage('logo') }}" alt="" class="preview-image"></p>
                        <input type="file" class="form-control-file" name="logo">
                    </div>
                </div>
                <div class="col-md-4">
                    <p class="text-bold text-md-right mt-1">Адрес</p>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" class="form-control" name="address" value="{{ $site->address }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <p class="text-bold text-md-right mt-1">Телефон</p>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" class="form-control" name="phone" value="{{ $site->phone }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <p class="text-bold text-md-right mt-1">E-mail</p>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" class="form-control" name="email" value="{{ $site->email }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <p class="text-bold text-md-right mt-1">Баннер (Изображение)</p>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <p><img src="{{ $site->getImage('banner_image') }}" alt="" class="preview-image"></p>
                        <input type="file" class="form-control-file" name="banner_image">
                    </div>
                </div>
                <div class="col-md-4">
                    <p class="text-bold text-md-right mt-1">Баннер (Текст)</p>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <textarea class="form-control js-editor" name="banner_text">{{ $site->banner_text }}</textarea>
                    </div>
                </div>
                <div class="col-md-4">
                    <p class="text-bold text-md-right mt-1">О компании (Изображение)</p>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <p><img src="{{ $site->getImage('about_us_image') }}" alt="" class="preview-image"></p>
                        <input type="file" class="form-control-file" name="about_us_image">
                    </div>
                </div>
                <div class="col-md-4">
                    <p class="text-bold text-md-right mt-1">О компании (Текст)</p>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <textarea class="form-control js-editor" name="about_us_text">{{ $site->about_us_text }}</textarea>
                    </div>
                </div>
                <div class="col-md-4">
                    <p class="text-bold text-md-right mt-1">О компании (Внизу)</p>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <textarea class="form-control" name="footer_text" rows="2">{{ $site->footer_text }}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn bg-gradient-success">Сохранить</button>
                    <a href="/admin" class="btn bg-gradient-secondary">Отменить</a>
                </div>
            </div>
        </div>
    </div>
{{ Form::close() }}
@endsection