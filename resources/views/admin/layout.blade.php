<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $title }}</title>
    <meta name="description" content="description">
    <meta name="keywords" content="keywords">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link href="/css/admin.css?<?=time()?>" rel="stylesheet">
    <link href="/css/main.css?<?=time()?>" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed">
<div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="nav-link" data-widget="pushmenu"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="/admin" class="nav-link">Главная</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="{{ route('settings.index') }}" class="nav-link">Настройки</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="{{ route('services.index') }}" class="nav-link">Услуги и цены</a>
            </li>
            <li class="nav-item d-none">
                <a href="{{ route('feedbacks.index') }}" class="nav-link">Веб-формы</a>
            </li>
        </ul>
        <a href="{{ route('logout') }}" class="ml-auto mr-3">Выйти</a>
    </nav>
    @include('admin.sidebar')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <h1 class="mb-2">{{ $title }}</h1>
            </div>
        </section>
        <div class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
    </div>
    <footer class="main-footer">
        &copy; {{ $site->name }}, 2020. Все права защищены.
    </footer>
</div>
<script src="/js/admin.js?<?=time()?>"></script>
<script src="/js/main.js?<?=time()?>"></script>
</body>
</html>