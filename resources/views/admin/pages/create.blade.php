@extends('admin.layout', ['title' => 'Новая страница - Добавление'])

@section('content')
{{ Form::open(['route' => 'pages.store', 'files' => true]) }}
    <div class="row">
        <div class="col-md-6">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Информация</h3>
                </div>
                <div class="card-body">
                    @include('admin.errors')
                    <div class="form-group">
                        <div class="icheck-primary">
                            {{ Form::checkbox('status', 1, true, ['id' => 'checkbox-status']) }}
                            <label for="checkbox-status">Активность</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Название</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                    </div>
                    <div class="form-group">
                        <label>Раздел</label>
                        {{ Form::select('parent_id', $pages, null, ['class' => 'form-control js-select']) }}
                    </div>
                    <div class="form-group">
                        <label>Сортировка</label>
                        <input type="text" class="form-control" name="sort" value="500">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Мета-теги</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>Заголовок браузера</label>
                        <input type="text" class="form-control" name="seo_title" value="{{ old('seo_title') }}">
                    </div>
                    <div class="form-group">
                        <label>Мета Описание</label>
                        <textarea class="form-control" name="seo_description" rows="2">{{ old('seo_description') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Ключевые слова</label>
                        <input type="text" class="form-control" name="seo_keywords" value="{{ old('seo_keywords') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Контент</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <textarea class="form-control js-editor" name="content">{{ old('content') }}</textarea>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn bg-gradient-success">Сохранить</button>
                    <a href="{{ route('pages.index') }}" class="btn bg-gradient-secondary">Отменить</a>
                </div>
            </div>
        </div>
    </div>
{{ Form::close() }}
@endsection