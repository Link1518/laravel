@extends('admin.layout', ['title' => 'Страницы сайта'])

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <a href="{{ route('pages.create') }}" class="btn bg-gradient-success">Добавить</a>
            </div>
            <div class="card-body">
                <table id="list" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <th>Символьный код</th>
                            <th>Сортировка</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pages as $page)
                        <tr>
                            <td>{{ $page->id }}</td>
                            <td><a href="{{ route('pages.edit', $page->id) }}">{{ $page->name }}</a></td>
                            <td>{{ $page->code }}</td>
                            <td>{{ $page->sort }}</td>
                            <td>
                                <a href="{{ route('pages.edit', $page->id) }}" class="btn bg-gradient-warning"
                                    title="Изменить"
                                >
                                    <i class="fas fa-edit"></i>
                                </a>
                                <a href="#" class="btn bg-gradient-danger js-remove"
                                    data-url="{{ route('pages.destroy', $page->id) }}"
                                    title="Удалить"
                                >
                                    <i class="fas fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection