<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Авторизация</title>
    <meta name="description" content="description">
    <meta name="keywords" content="keywords">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link href="/css/admin.css?<?=time()?>" rel="stylesheet">
    <link href="/css/main.css?<?=time()?>" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="/">{{ $site->name }}</a>
    </div>
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Пожалуйста, авторизуйтесь</p>
            {{ Form::open(['route' => 'loginAuth']) }}
                @if (session('error'))
                <div class="alert alert-danger">
                    <p class="mb-0">{{ session('error') }}</p>
                </div>
                @endif
                @include('admin.errors')
                <div class="input-group mb-3">
                    <input type="text" name="email" class="form-control" placeholder="E-mail">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="password" name="password" class="form-control" placeholder="Пароль">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="icheck-primary">
                            {{ Form::checkbox('remember', 1, null, ['id' => 'checkbox-remember']) }}
                            <label for="checkbox-remember">Запомнить меня</label>
                        </div>
                    </div>
                    <div class="col-4">
                        <button type="submit" class="btn bg-gradient-primary btn-block">Войти</button>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
<script src="/js/admin.js?<?=time()?>"></script>
<script src="/js/main.js?<?=time()?>"></script>
</body>
</html>