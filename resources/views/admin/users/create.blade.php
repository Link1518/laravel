@extends('admin.layout', ['title' => 'Новый пользователь'])

@section('content')
{{ Form::open(['route' => 'users.store']) }}
    <div class="row">
        <div class="col-md-6">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Добавление</h3>
                </div>
                <div class="card-body">
                    @include('admin.errors')
                    <div class="form-group">
                        <label>Имя</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                    </div>
                    <div class="form-group">
                        <label>E-mail</label>
                        <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                    </div>
                    <div class="form-group">
                        <label>Пароль</label>
                        <input type="password" class="form-control" name="password">
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn bg-gradient-success">Сохранить</button>
                    <a href="{{ route('users.index') }}" class="btn bg-gradient-secondary">Отменить</a>
                </div>
            </div>
        </div>
    </div>
{{ Form::close() }}
@endsection