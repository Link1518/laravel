@extends('admin.layout', ['title' => 'Пользователи'])

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <a href="{{ route('users.create') }}" class="btn bg-gradient-success">Добавить</a>
            </div>
            <div class="card-body">
                <table id="list" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Имя</th>
                            <th>E-mail</th>
                            <th>Активность</th>
                            <th>Администратор</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td><a href="{{ route('users.edit', $user->id) }}">{{ $user->name }}</a></td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->isActive() }}</td>
                            <td>{{ $user->isAdmin() }}</td>
                            <td>
                                <a href="{{ route('users.edit', $user->id) }}" class="btn bg-gradient-warning"
                                    title="Изменить"
                                >
                                    <i class="fas fa-edit"></i>
                                </a>
                                <a href="#" class="btn bg-gradient-danger js-remove"
                                    data-url="{{ route('users.destroy', $user->id) }}"
                                    title="Удалить"
                                >
                                    <i class="fas fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection