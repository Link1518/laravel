@extends('admin.layout', ['title' => $user->name])

@section('content')
{{ Form::open(['route' => ['users.update', $user->id], 'method' => 'put']) }}
    <div class="row">
        <div class="col-md-6">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Редактирование</h3>
                </div>
                <div class="card-body">
                    @include('admin.errors')
                    <div class="form-group">
                        <div class="icheck-primary d-inline">
                            {{ Form::checkbox('status', 1, $user->status, ['id' => 'checkbox-status']) }}
                            <label for="checkbox-status">Активность</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Имя</label>
                        <input type="text" class="form-control" name="name" value="{{ $user->name }}">
                    </div>
                    <div class="form-group">
                        <label>E-mail</label>
                        <input type="text" class="form-control" name="email" value="{{ $user->email }}">
                    </div>
                    <div class="form-group">
                        <label>Пароль</label>
                        <input type="password" class="form-control" name="password">
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn bg-gradient-success">Сохранить</button>
                    <a href="{{ route('users.index') }}" class="btn bg-gradient-secondary">Отменить</a>
                </div>
            </div>
        </div>
    </div>
{{ Form::close() }}
@endsection