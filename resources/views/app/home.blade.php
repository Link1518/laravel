@extends('app.layout', [
    'title' => 'Главная',
    'description' => '',
    'keywords' => ''
])

@section('content')
<div class="services">
    <div class="services-bg" style="background-image: url('{{ $site->getImage('banner_image') }}');">
        <div class="container">
            <div class="services-text-bg">
                {!! $site->banner_text !!}
            </div>
        </div>
    </div>
</div>
<div class="about-us">
    <div class="container">
        <div class="section-title">
            <span>
                <h2 class="section-title-text">О компании</h2>
            </span>
        </div>
        <div class="about-us-row">
            <div class="about-us-col">
                <img src="{{ $site->getImage('about_us_image') }}" alt="">
            </div>
            <div class="about-us-col">
                <div class="about-us-text">
                    {!! $site->about_us_text !!}
                </div>
                <div class="about-us-button">
                    <button type="button" class="my-3" onclick="location.href='/o-kompanii';">Подробнее</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="price">
    <div class="price-bg">
        <div class="container">
            <div class="section-title">
                <span>
                    <h2 class="section-title-text">Услуги</h2>
                </span>
            </div>
            <div class="price-row">
                @foreach ($services as $service)
                <div class="price-col">
                    <img src="{{ $service->getImage('image') }}" alt="">
                    <p class="product-name mb-0">{{ $service->name }}</p>
                    @if ($service->price)
                    <p class="product-price mb-0">{{ $service->price }}</p>
                    @endif
                </div>
                @endforeach
            </div>
        </div>
        <div class="section-title">
            <span>
                <h2 class="section-title-text">Напишите нам</h2>
            </span>
        </div>
        <section class="feedback">
            <div class="feedback-bg">
                <div class="container">
                    <div class="feedback-wrapper">
                        <div class="feedback-form">
                            <form class="js-feedback">
                                <input type="text" name="name" placeholder="Ваше имя" required>
                                <input type="text" name="phone" placeholder="Номер телефона" required>
                                <label>Комментарий</label>
                                <textarea name="comment" rows="5" required></textarea>
                                <button type="submit">Отправить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="section-title">
    <span>
        <h2 class="section-title-text">Наши координаты</h2>
    </span>
</div>
<div class="address">
    <div class="address-row">
        <div class="address-col">
            <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A3b25ece13861aca466a173744f27e1bcc78d7655bd8190aedb136b273fc876bd&amp;width=100%&amp;height=600&amp;lang=ru_RU&amp;scroll=false"></script>
        </div>
        <div class="address-col">
            <div class="address-form">
                <form class="js-feedback">
                    <img src="{{ $site->getImage('logo') }}" alt="">
                    <p class="mb-0">
                        <i class="fa fa-phone"></i>Позвоните нам по номеру: {{ $site->phone }}
                    </p>
                    <p class="mb-0">
                        <i class="fa fa-envelope"></i>Напишите нам на почту: {{ $site->email }}
                    </p>
                    <hr>
                    <div class="input-group">
                        <div class="input-group-item">
                            <i class="fa fa-user"></i><input type="text" class="js-name" name="name"
                                placeholder="Ваше имя" required>
                        </div>
                        <div class="input-group-item">
                            <i class="fa fa-phone"></i><input type="text" class="js-phone" name="phone"
                                placeholder="Номер телефона" required>
                        </div>
                    </div>
                    <button type="submit">Заказать звонок</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection