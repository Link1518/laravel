<!DOCTYPE html>
<html lang="ru" class="h-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $title }}</title>
    <meta name="description" content="{{ $description }}">
    <meta name="keywords" content="{{ $keywords }}">
    <link href="/css/app.css?<?=time()?>" rel="stylesheet">
    <link href="/css/site.css?<?=time()?>" rel="stylesheet">
</head>
<body class="d-flex flex-column h-100">
<header>
    <nav>
        <div class="container">
            <input type="checkbox" id="checkbox-menu">
            <label for="checkbox-menu">
                <ul class="menu touch">
                    <?php $i = 0; ?>
                    <?php while ($i < 8) : ?>
                    <?php if ($i > 0) : ?>
                    <hr class="vertical-nav-line">
                    <?php endif; ?>
                    <li>
                        <a href="<?= $menu[$i]['href'] ?>"><?= $menu[$i]['name'] ?></a>
                        <a href="<?= $menu[$i + 1]['href'] ?>"><?= $menu[$i + 1]['name'] ?></a>
                    </li>
                    <?php $i += 2; ?>
                    <?php endwhile; ?>
                </ul>
                <span class="toggle">☰</span>
            </label>
        </div>
    </nav>
    <section class="head-block">
        <div class="container">
            <div class="head-block-wrapper">
                <div class="head-block-col">
                    <div class="head-logo">
                        <a href="/"><img src="{{ $site->getImage('logo') }}" alt=""></a>
                    </div>
                    <div class="head-info">
                        <p class="mb-0" style="width: 140px;">{{ $site->name }}<br>{{ $site->address }}</p>
                    </div>
                </div>
                <div class="head-block-col">
                    <div class="header-number"><p class="mb-0">{{ $site->phone }}</p></div>
                    <div class="header-button">
                        <button type="button" data-toggle="modal" data-target="#feedback-1">Обратный звонок</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</header>
<main class="flex-shrink-0">
    @yield('content')
</main>
<footer class="mt-auto">
    <div class="footer-bg">
        <div class="container">
            <div class="back-to-top-wrapper">
                <a href="#" class="back-to-top" title="Наверх"></a>
            </div>
            <div class="footer-row">
                <div class="footer-col">
                    <ul>
                        <li><h3 class="company-name">{{ $site->name }}</h3></li>
                        <li style="white-space: pre;">{{ $site->footer_text }}</li>
                        <li><p class="mb-0">{{ $site->name }}</p><p class="mb-0 ml-3">{{ $site->phone }}</p></li>
                    </ul>
                </div>
                <div class="footer-col">
                    <?php $i = 0; ?>
                    <?php while ($i < 8) : ?>
                    <?php if ($i > 0) : ?>
                    <hr class="footer-vertical-line my-0">
                    <?php endif; ?>
                    <ul class="mb-0">
                        <li><a href="<?= $menu[$i]['href'] ?>"><?= $menu[$i]['name'] ?></a></li>
                        <li><a href="<?= $menu[$i + 1]['href'] ?>"><?= $menu[$i + 1]['name'] ?></a></li>
                        <li><a href="<?= $menu[$i + 2]['href'] ?>"><?= $menu[$i + 2]['name'] ?></a></li>
                        <li><a href="<?= $menu[$i + 3]['href'] ?>"><?= $menu[$i + 3]['name'] ?></a></li>
                    </ul>
                    <?php $i += 4; ?>
                    <?php endwhile; ?>
                </div>
            </div>
            <div class="comp-logo">
                <a href="https://it-influx.ru/">Сайт разработан<br>компанией it-influx</a>
            </div>
        </div>
    </div>
</footer>
@include('app.modal')
<script src="/js/app.js?<?=time()?>"></script>
<script src="/js/site.js?<?=time()?>"></script>
</body>
</html>