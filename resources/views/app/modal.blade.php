<div id="feedback-1" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form class="modal-content js-feedback">
            <div class="modal-header">
                <h5 class="modal-title">Заказать звонок</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Ваше имя</label>
                    <input type="text" class="form-control js-name" name="name" placeholder="Ваше имя" required>
                </div>
                <div class="form-group">
                    <label>Номер телефона</label>
                    <input type="text" class="form-control js-phone" name="phone" placeholder="Номер телефона" required>
                </div>
                <div class="form-group">
                    <label>Удобное время звонка</label>
                    <input type="text" class="form-control" name="time" placeholder="Удобное время звонка">
                </div>
                <div class="form-group">
                    <label class="mb-0"><input type="checkbox" name="personal" checked disabled> Я согласен на обработку персональных данных</label>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="submit" class="btn btn-primary">Отправить</button>
            </div>
        </form>
    </div>
</div>