@extends('app.layout', [
    'title' => $page->seo_title ?: $page->name,
    'description' => $page->seo_description,
    'keywords' => $page->seo_keywords
])

@section('content')
<div class="container">
    <div class="content">
        <h1 class="title">{{ $page->name }}</h1>
        @if ($page->content)
        {!! $page->content !!}
        @endif
        <?php foreach ($page->getChilds() as $child): ?>
        <div class="list-group list-group-custom">
            <a href="{{ $child['href'] }}" class="list-group-item list-group-item-action">{{ $child['name'] }}</a>
        </div>
        <?php endforeach ?>
    </div>
</div>
@endsection